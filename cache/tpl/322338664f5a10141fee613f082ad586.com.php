<!DOCTYPE html>
<html>
    <head>
        <title><?= $this->title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="public/css/style.css">
            </head>
    <body>
        <div id="wrapper">
            <h1>Heading1</h1>
<table>
    <thead>
        <th>ID</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Age</th>
    </thead>
    <tbody>
        <?php foreach($this->users as $user){ ?>
        <tr>
            <td><?php echo $user->getId(); ?></td>
            <td><?php echo $user->getfirstname(); ?></td>
            <td><?php echo $user->getLastname(); ?></td>
            <td><?php echo $user->getAge(); ?></td>
        </tr>
       <?php 
       
       } ?>
    </tbody>
</table>        </div>
        <?php
        $cb = \rueckgrat\xhr\CallbackManager::getCallbacks();
        if (count($cb) > 0) {
            echo '<script type="text/javascript"">$(document).ready(function(){handleCallbacks(' . json_encode(array('callbacks' => $cb)) . ');});</script>' . "\n";
        }
        ?>
                <script type="text/javascript" src="public/js/main.js"></script>
    </body>
</html>