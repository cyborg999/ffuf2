<?php
namespace app\dbo;

/**
 * Description of User
 *
 * @author Lenovo G470
 */
class User extends \rueckgrat\db\Mapper {
    protected $firstname;
    protected $lastname;
    protected $age;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function getFirstname(){
        return $this->firstname;
    }
    
    public function getLastname(){
        return $this->lastname;
    }
    
    public function getAge(){
        return $this->age;
    }
}
