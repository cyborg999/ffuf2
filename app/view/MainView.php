<?php
namespace app\view;
/**
 * Description of MainView
 *
 * @author Lenovo G470
 */
class MainView  extends \rueckgrat\mvc\FastView {
    protected $users;
    public function __construct(){
        parent::__construct();
        
        $this->cacheDisabled = TRUE;
        $this->title = 'FFUF Testing';
    }
    
    public function renderFrontPage($users){
        $this->users = $users;
        $this->pageContent = $this->getCompiledTpl('main/main');
        
        return $this->renderMainPage();
    }
    
}
