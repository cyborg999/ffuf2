<?php
namespace app\model;

/**
 * Description of userModel
 *
 * @author Lenovo G470
 */
class userModel extends \rueckgrat\mvc\DefaultDBModel {
    public function __construct(){
        parent::__construct('user');
    }
    
    public function getAllUsers(){
        $users      = array();
        $sql        = "SELECT * FROM user";
        $records    = $this->db->query($sql);
        
        while($row = $records->fetch()){
            $user = new \app\mapper\User();
            $user->map($row);
            $users[] = $user;
        }
        
        return $users;
    }
}
