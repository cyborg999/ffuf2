<?php
namespace app\controller;

use rueckgrat\mvc\DefaultController;

/**
 * Description of Main
 *
 * @author Lenovo G470
 */
class Main extends DefaultController {
    protected $userModel;
    protected $mainView;
    
    public function __construct(){
        parent::__construct();
        
        $this->userModel = new \app\model\UserModel();
        $this->mainView = new \app\view\MainView();
    }
    
    public function index(){
        echo 'index';
    }
    public function test(){
        $users = $this->userModel->getAllUsers();
        return $this->mainView->renderFrontPage($users);
    }
}
